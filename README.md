# widevine-debian

## Description

A simple script in bash to install Widevine Content Decryption Module (WidevineCdm) on Debian for Chromium.
It's been tested on Debian Testing "bullseye" with Chromium version 78.0.3904.97

Widevine is a "Content Descryption Module" (CDM) to play Encrypted Media
Extensions (EME).

Example of website that need it:
netflix.com

## Installation

Download and run install.sh as root

## Changelog
### v0.1 - 20/1/2020
- First release of script, also some basic checks to run script smooth and report
  errors were implemented.
