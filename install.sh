#!/bin/bash

# Simple script to check and install Widevine Content Decryption Module for Chromium on Debian
# Version: 0.1

temp_dir="/tmp/winevine/"
old_dir=$(pwd)
file_name="chrome.deb"
browser=$(which chromium)

# colors
RED='\033[0;31m'
NC='\033[0m'

error_() {
    echo -e "${RED}Exiting with error: $1 $NC"
    exit $?
}

check_sudo() {
    if [ $EUID -ne 0 ]; then
        error_ "Sudo privileges required"
    fi
}

check_distro() {
    if [ $(lsb_release -is) != 'Debian' ]; then
        error_ "Debian is required to run the script"
    fi
}

# Check if chromium is installed
check_chrome() {
    if [ ! -f $browser ]; then
        error_ "Chromium does not exist"
    fi
}

check_sudo
check_distro
check_chrome

# Check if already WidevineCdm installed
if [ -d /usr/lib/chromium/WidevineCdm ]; then
    echo "Widevine Content Decryption Module directory already exists."
    echo "If you are having problem with WidevineCdm and trying to reinstall delete directory and run again the script."
    echo "Directory: /usr/lib/chromium/WidevineCdm"
    error_ "Installation directory exists"
fi

# create download dir
if [ ! -d $temp_dir ]; then
    mkdir -pv $temp_dir || error_ "Cannot create temporary directory for download"
    cd $temp_dir
fi

# -O for new file name
wget -O $file_name https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb || error_ "Problem downloading file"

# Extract WidevineCdm
ar x $file_name || error_ "Unable to extract downloaded .deb file"
tar xvJf data.tar.xz || error_ "Unable to extract file from .deb file"
mv opt/google/chrome/WidevineCdm/ /usr/lib/chromium/ || error_ "Error moving WidevineCdm directory to Chromium folder"

cd $old_dir

# Restart Chromium and test WidevineCdm
if [ $(pidof $browser) ]; then
    echo -n "Whould you like to restart your browser now and test? [y/n]: "
    read option
    if [ $option == "Y" ] || [ $option == "y" ]; then
        killall $(pidof $browser)
        $browser https://bitmovin.com/demos/drm
    elif [ $option == "N" ] || [ $option == "n" ]; then
        exit 0
    fi
fi
